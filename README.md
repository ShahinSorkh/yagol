# Yet Another Game of Life
![https://gitlab.com/shahinsorkh/yagol](https://gitlab.com/ShahinSorkh/yagol/badges/master/pipeline.svg)

Read more about [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

This package comes in two forms:
- [written in C++](./cpp)
- [written in Rust](./rust) _WIP_


## C++ version

### Build and run

Make sure you have `cmake` and `ncurses` installed.

```sh
$ mkdir build && cd build
$ cmake ..
$ make && ./yagol
```

#### Or using docker

```sh
$ docker run --rm -it registry.gitlab.com/shahinsorkh/yagol/cpp:master
```

## Rust version

_THIS IS A WORK IN PROGRESS YET_

