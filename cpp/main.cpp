#include <iostream>
#include <csignal>

#ifdef _WIN32
#   include <Windows.h>
#else
#   include <unistd.h>
#endif

#include "colors.hpp"
#include "world.hpp"

bool RUNNING = true;

void signal_callback_handler(int signum) {
    RUNNING = false;
}

int main() {
    std::signal(SIGINT, signal_callback_handler);
    std::signal(SIGTERM, signal_callback_handler);

    initscr();
    init_colors();

    scrollok(stdscr, false);

    int h, w;
    getmaxyx(stdscr, h, w);

    gof::world world(w, h);

    while (RUNNING) {
        world.draw();
        world.evolute();
        usleep(300000);
    }

    endwin();
    return 0;
}
