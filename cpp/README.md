# Yet Another Game of Life - C++

## Build and run

Make sure you have `cmake` and `ncurses` installed.

```sh
$ mkdir build && cd build
$ cmake ..
$ make && ./yagol
```

### Or using docker

```sh
$ docker run --rm -it registry.gitlab.com/shahinsorkh/yagol/cpp:master
```

