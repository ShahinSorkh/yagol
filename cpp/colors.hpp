#ifndef GOF_COLORS_HPP
#define GOF_COLORS_HPP

#include <curses.h>

#define COLOR_PAIR_ALIVE        0b10000
#define COLOR_PAIR_DEAD         0b10001
#define COLOR_PAIR_ALIVE_COUNT  0b10010
#define COLOR_PAIR_DEAD_COUNT   0b10011

void init_colors();

#endif // GOF_COLORS_HPP
