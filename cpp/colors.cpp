#ifndef GOF_COLORS_CPP
#define GOF_COLORS_CPP

#include <iostream>
#include "colors.hpp"

void init_colors() {
    if (has_colors() == false) {
        using namespace std;
        endwin();
        cerr << "Your terminal does not support colors" << endl;
        exit(1);
    }

    start_color();
    init_pair(COLOR_PAIR_ALIVE, COLOR_BLACK, COLOR_BLUE);
    init_pair(COLOR_PAIR_DEAD, COLOR_BLACK, COLOR_GREEN);
    init_pair(COLOR_PAIR_ALIVE_COUNT, COLOR_RED, COLOR_BLACK);
    init_pair(COLOR_PAIR_DEAD_COUNT, COLOR_RED, COLOR_BLACK);
}

#endif // GOF_COLORS_CPP
