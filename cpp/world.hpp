#ifndef GOF_WORLD_HPP
#define GOF_WORLD_HPP

#include <ctime>
#include <functional>

#include "colors.hpp"

namespace gof {

struct world_stats {
    unsigned int alives_count;
    unsigned int deads_count;
};

class world {
    private:
        int width;
        int height;
        bool** cells;
        int calculate_alive_neighbours(int, int);
        bool** generate_world(::std::function<bool(int, int)>);
        world_stats calculate_stats();
    public:
        world() = delete;
        world(int, int);
        void evolute();
        void draw();
};

} // namespace gof

#endif // GOF_WORLD_HPP
