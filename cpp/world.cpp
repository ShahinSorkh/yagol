#ifndef GOF_WORLD_CPP
#define GOF_WORLD_CPP

#include <string>
#include "world.hpp"

namespace gof {

world::world(int w, int h) : width(w), height(h) {
    using namespace std;
    srand(time(nullptr));
    cells = generate_world([] (int i, int j) { return (rand() % 100) < 10; });
}

bool** world::generate_world(::std::function<bool(int, int)> generator) {
    bool** new_cells = new bool* [height];
    for (int i = 0; i < height; i++) {
        new_cells[i] = new bool[width];
        for (int j = 0; j < width; j++) {
            new_cells[i][j] = generator(i, j);
        }
    }
    return new_cells;
}

int world::calculate_alive_neighbours(int y, int x) {
    int sum = 0;
    for (int i = -1; i < 2; i++) {
        for (int j = -1; j < 2; j++) {
            if (i == 0 && j == 0) continue;

            int neighbour_y = (y + height + i) % height;
            int neighbour_x = (x + width + j) % width;

            bool neighbour = cells[neighbour_y][neighbour_x];
            sum += neighbour ? 1 : 0;
        }
    }
    return sum;
}

void world::draw() {
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            bool is_alive = cells[i][j];
            auto color_pair = COLOR_PAIR(is_alive ? COLOR_PAIR_ALIVE : COLOR_PAIR_DEAD);

            attron(color_pair);
            mvaddch(i, j, ' ');
            attroff(color_pair);
        }
    }

    using namespace std;
    world_stats stats = calculate_stats();

    const char* alives = string("ALIVE: " + to_string(stats.alives_count)).c_str();
    attron(COLOR_PAIR(COLOR_PAIR_ALIVE_COUNT));
    mvaddstr(0, 0, alives);
    attroff(COLOR_PAIR(COLOR_PAIR_ALIVE_COUNT));

    const char* deads = string("DEAD:  " + to_string(stats.deads_count)).c_str();
    attron(COLOR_PAIR(COLOR_PAIR_DEAD_COUNT));
    mvaddstr(1, 0, deads);
    attroff(COLOR_PAIR(COLOR_PAIR_DEAD_COUNT));

    move(2, 0);
    refresh();
}

void world::evolute() {
    cells = generate_world([this] (int i, int j) {
            bool is_alive = cells[i][j];
            int alive_neighbours = calculate_alive_neighbours(i, j);

            if (is_alive && alive_neighbours < 2) return false;
            if (is_alive && alive_neighbours > 3) return false;
            if (!is_alive && alive_neighbours == 3) return true;

            return is_alive;
            });
}

world_stats world::calculate_stats() {
    world_stats stats;
    stats.alives_count = 0;
    stats.deads_count = 0;
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            bool is_alive = cells[i][j];
            if (is_alive) stats.alives_count++;
            else stats.deads_count++;
        }
    }
    return stats;
}

} // namespace gof

#endif // GOF_WORLD_CPP
